<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 17.11.2017
 * Time: 6:45
 */

namespace PhoneBook;


class Book extends View{
    public function add($data) {
        //обновление кеша перед return
        global $PhoneBook;
        return $PhoneBook->DB->insert('phonebook', array(
            'user' => $PhoneBook->User->Session->id,
            'name' => $data['name'],
            'surname' => $data['surname'],
            'photo' => $data['photo'],
            'email' => $data['email'],
            'telephone' => $data['telephone']
        ));
    }
    public function read($data) {
        global $PhoneBook;
        //можно было бы доставать из кэша
        $return = $PhoneBook->DB->select('phonebook', array(
            'user' => $PhoneBook->User->Session->id,
            'id' => $data['id']
        ), 1);
        if (is_object($return)){
            return $return->fetch(\PDO::FETCH_ASSOC);
        }
        return array();
    }
    public function getAll() {
        global $PhoneBook;
        //тут можно кэшить по 'user' => $PhoneBook->User->Session->id но задания не было
        $return = $PhoneBook->DB->select('phonebook', array(
            'user' => $PhoneBook->User->Session->id
        ));
        if (is_object($return)){
            return $return->fetchAll(\PDO::FETCH_ASSOC);
        }
        return array();
    }

    public function edit($data) {
        global $PhoneBook;
        //обновление кеша перед return
        $where = array(
            'user' => $PhoneBook->User->Session->id,
            'id' => $data['id']
        );
        unset($data['id']);
        unset($data['user']);
        return $PhoneBook->DB->update('phonebook',$data, $where, 1);
    }
    public function drop($data) {
        global $PhoneBook;
        //обновление кеша перед return
        return $PhoneBook->DB->delete('phonebook', array('user' => $PhoneBook->User->Session->id, 'id' => $data['id']), 1);
    }
}