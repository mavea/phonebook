<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 17.11.2017
 * Time: 6:49
 */

namespace PhoneBook\Book;


class Controller extends \PhoneBook\Book{
    public function Add($data){
        global $PhoneBook;
        $View = $PhoneBook->View;
        $Typo = $PhoneBook->Common->Typo;
        if ($PhoneBook->User->isAuth()){
            switch (false){
                case $data['email'] && $Typo->checkMail($data['email']):
                    $View->setMessage('status', 406);
                    $View->setMessage('statusType', 'danger');
                    $View->setMessage('message', 'Емаил ' . $data['email'] . ' не приемлем');
                    break
                    ;
                default:
                    if ($id = $PhoneBook->Book->add($data)) {
                        if (isset($_FILES) && count($_FILES)){
                            if ($image = $PhoneBook->File->uploadImage($_FILES, \PhoneBook\Conf::DIR_UPLOAD_IMAGE, $PhoneBook->Common->Typo->crypt(array(
                                'id' => $id,
                                'bookID' => $PhoneBook->User->Session->id
                            )),  array('image/jpeg', 'image/png'))){
                                $data['photo'] = $image[0];
                                if ($PhoneBook->Book->edit($data)){
                                    $View->setMessage('status', 200);
                                    $View->setMessage('statusType', 'success');
                                    $View->setMessage('message', 'Контакт сохранен');
                                    exit;
                                }
                            }
                            $View->setMessage('status', 406);
                            $View->setMessage('statusType', 'danger');
                            $View->setMessage('message', 'Ошибка сохранения изображения');
                            return false;
                        }
                        $View->setMessage('status', 200);
                        $View->setMessage('statusType', 'success');
                        $View->setMessage('message', 'Контакт сохранен');
                    } else {
                        $View->setMessage('status', 406);
                        $View->setMessage('statusType', 'danger');
                        $View->setMessage('message', 'Ошибка при сохранении контакта');
                    }
                    break;
            }
        } else {
            $View->setMessage('status', 406);
            $View->setMessage('statusType', 'danger');
            $View->setMessage('message', 'Вы не авторизованы');
        }
    }
    public function listBook($pattern=''){
        global $PhoneBook;
        $View = $PhoneBook->View;
        $phones = $PhoneBook->Book->getAll();
        $return='';
        if ($pattern){
            foreach ($phones as $i){
                $return .= $View->viewPage($pattern, new \PhoneBook\Book\Init($i));
            }
            return $return;
        }else{
            $View->setMessage('result', $phones);
        }
        return $phones;
    }
    public function Read($data){
        global $PhoneBook;
        $View = $PhoneBook->View;
        if ($PhoneBook->User->isAuth()){
            $return = $PhoneBook->Book->read($data);
            $return['telephoneText'] = print_r($PhoneBook->Common->Typo->phoneToText($return['telephone']),1)/*$PhoneBook->Common->Typo->phoneToText($return['telephone'])*/;
            return $return;
        } else {
            $View->setMessage('status', 406);
            $View->setMessage('statusType', 'danger');
            $View->setMessage('message', 'Вы не авторизованы');
        }
        return null;
    }
    public function Edit($data){
        global $PhoneBook;
        $View = $PhoneBook->View;
        if ($PhoneBook->User->isAuth()){
            if (isset($_FILES) && count($_FILES)){
                if ($image = $PhoneBook->File->uploadImage($_FILES, \PhoneBook\Conf::DIR_UPLOAD_IMAGE, $PhoneBook->Common->Typo->crypt(array(
                    'id' => $data['id'],
                    'bookID' => $PhoneBook->User->Session->id
                )), array('image/jpeg', 'image/png'))){
                    $data['photo'] = $image[0];
                }else{
                    $View->setMessage('status', 406);
                    $View->setMessage('statusType', 'danger');
                    $View->setMessage('message', 'Ошибка сохранения изображения');
                    return false;
                }
            }
            if (isset($data['photo']) && !$data['photo']){
                unset($data['photo']);
            }
            if ($PhoneBook->Book->edit($data)){
                $View->setMessage('status', 200);
                $View->setMessage('statusType', 'success');
                $View->setMessage('message', 'Контакт изменен');
            } else {
                $View->setMessage('status', 406);
                $View->setMessage('statusType', 'danger');
                $View->setMessage('message', 'Ошибка сохранения');
            }
        } else {
            $View->setMessage('status', 406);
            $View->setMessage('statusType', 'danger');
            $View->setMessage('message', 'Вы не авторизованы');
        }
        return ;

    }
    public function Drop($data){
        global $PhoneBook;
        $View = $PhoneBook->View;
        if ($PhoneBook->User->isAuth()){
            if ($PhoneBook->Book->drop($data)){
                $View->setMessage('status', 200);
                $View->setMessage('statusType', 'success');
                $View->setMessage('message', 'Контакт удален');
            } else {
                $View->setMessage('status', 406);
                $View->setMessage('statusType', 'danger');
                $View->setMessage('message', 'Ошибка удаления');
            }
        } else {
            $View->setMessage('status', 406);
            $View->setMessage('statusType', 'danger');
            $View->setMessage('message', 'Вы не авторизованы');
        }
        return ;

    }
}