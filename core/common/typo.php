<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 16.11.2017
 * Time: 19:58
 */

namespace PhoneBook\Common;


class Typo extends \PhoneBook\Pattern\Singleton{    static protected $NamingSystemBigNumbers = array(
    1=>array(
        '',
        'один',
        'два',
        'три',
        'четыре',
        'пять',
        'шесть',
        'семь',
        'восемь',
        'девять',
    ),
    2=>array(
        '',
        'одиннадцать',
        'двенадцать',
        'тринадцать',
        'четырнадцать',
        'пятнадцать',
        'шестнадцать',
        'семнадцать',
        'восемнадцать',
        'девятнадцать',),
    10=>array(
        '',
        '',
        'двадцать',
        'тридцать',
        'сорок',
        'пятьдесят',
        'шестьдесят',
        'семьдесят',
        'восемьдесят',
        'девяносто',),
    100=>array(
        '',
        'сто',
        'двести',
        'триста',
        'четыреста',
        'пятьсот',
        'шестьсот',
        'семьсот',
        'восемьсот',
        'девятьсот',),
    1000=>array(
        'тысяча',
        'тысячи',
        'тысяч'
    ),
    1000000=>array(
        'миллион',
        'миллиона',
        'миллионов'
    ),
    1000000000=>array(
        'миллиард',
        'миллиарда',
        'миллиардов'
    ),

);

    public function checkLogin($login){
        if (preg_match('/[a-zA-Z]/u', $login) && preg_match('/[0-9]/u', $login)){
            if (preg_match('/[^a-zA-Z0-9]/u', $login)){
                return false;
            }
            return true;
        }
        return false;
    }
    public function checkMail($mail){
        if (preg_match('/^[^@]+@[^@\.]+\.[^@]+$/u', $mail)) {
            return true;
        }
        return false;
    }
    public function checkPassword($password)
    {
        if (preg_match('/[a-zA-Z]/u', $password) && preg_match('/[0-9]/u', $password)) {
            return true;
        }
        return false;
    }
    public function phoneToText($phone){
        $phone = preg_replace('/\\D/', '', (string)$phone);
        $return = '';
        $i=1;
        while ($phone){
            if (strlen($phone)>3){
                list($num,$phone)=array(substr($phone,-3),substr($phone,0,strlen($phone)-3));
            }else{
                list($num,$phone)=array($phone,'');
            }
            $return = $this->parseSmall($num) . ' ' . $this->countNuls($i, substr($num, -1)) . $return;
            $i*=1000;
        }
        return trim($return);
    }

    public function countNuls($i,$end)
    {
        $end=(integer)$end;
        if ($i>1){
            if ($end==1){
                return static::$NamingSystemBigNumbers[$i][0];
            }
            if ($end<5){
                return static::$NamingSystemBigNumbers[$i][1];
            }
            return static::$NamingSystemBigNumbers[$i][2];
        }
    }
    public function parseSmall($phone)
    {
        $return = '';
        if (strlen($phone)==3) {
            $return .= ' ' . static::$NamingSystemBigNumbers[100][$phone{0}];
            $phone = substr($phone, -2);
        }
        if (strlen($phone) == 2) {
            if ($phone{0} == '1'){
                $return .= ' ' . static::$NamingSystemBigNumbers[2][$phone{1}];
                return $return;
            }
            $return .= ' ' . static::$NamingSystemBigNumbers[10][$phone{0}];
            $phone = substr($phone, -1);
        }
        $return .= ' ' . static::$NamingSystemBigNumbers[1][$phone];
        return $return;
    }
    public function crypt($code)
    {
        return rtrim(base64_encode(json_encode($code)),'==');
    }
    public function decrypt($code)
    {
        return json_decode(base64_decode($code.'=='));
    }
}