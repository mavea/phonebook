<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 13.11.2017
 * Time: 15:13
 */

namespace PhoneBook;


class DB extends Pattern\Singleton{
    protected $pdo = null;
    protected function __construct ()
    {
        $this->pdo = new \PDO(Conf::DB_ADDRESS, Conf::DB_USER, Conf::DB_PASSWORD);
    }
    protected function buildLimit($limit)
    {
        if (is_array($limit)){
            return implode(', ', $limit);
        }
        return $limit;
    }
    protected function buildWhere($selectData)
    {
        if (is_array($selectData)){
            $logic='AND';
            $where = array();
            foreach($selectData as $key => $item) {

                if($key!='logic') {
                    if (is_numeric($key)&&is_array($item)) {
                        $where[] = '( ' . $this->buildWhere($item) . ' )';
                    }else{
                        preg_match('/^([^a-zA-Z0-9]*)([a-zA-Z0-9]+)$/', $key, $condition);
                        list($condition, $key) = array(($condition[1] ?: '='), $condition[2]);
                        if ($key == 'password'){
                            $item = md5($item);
                        }
                        switch ($condition) {
                            case '!@':
                            case '@':
                                $whereItem = array();
                                if (is_array($item)){
                                    foreach($item as $i){
                                        $whereItem[]=$this->pdo->quote($i);
                                    }
                                }
                                if ($condition=='#') {
                                    $where[] = '`' . $key . '` IN (' . implode(', ', $whereItem) . ' )';
                                }else{
                                    $where[] = '`' . $key . '` NOT IN (' . implode(', ', $whereItem) . ' )';
                                }
                                break;
                            case '!#':
                            case '#':
                                if ($condition=='#'){
                                    $where[] = '`' . $key . '` RLIKE \'' . $selectData[$key] . '\' ';
                                }else{
                                    $where[] = '`' . $key . '` NOT RLIKE \'' . $selectData[$key] . '\' ';

                                }
                                break;
                            default:
                                $selectData[$key] = $this->pdo->quote($item);
                                $where[] = '`' . $key . '`' . $condition . $selectData[$key] . ' ';
                                break;
                        }
                    }
                }else{
                    $logic = $item;
                }
            }
            return implode($logic.' ', $where);
        }
        return $selectData;
    }
    public function select($table, $selectData = array(), $limit = array(), $order = array(), $group = array() )
    {
        $sql = 'SELECT * FROM  `' . $table . '` ';
        if (count($selectData)){
            $sql .= 'WHERE '.$this->buildWhere($selectData);
        }
        /*
        if (count($order)){
            $sql .= 'Order '.$this->buildOrder($order);
        }
        if (count($group)){
            $sql .= 'WHERE '.$this->buildGroup($group);
        }*/
        if ((is_array($limit) && count($limit)) || $limit) {
            $sql .= 'LIMIT ' . $this->buildLimit($limit);
        }
        $sql .= ';';
        return $this->pdo->query($sql);
    }
    public function delete($table, $selectData = array(), $limit = array())
    {
        $sql = 'DELETE FROM `' . $table . '` ';
        if (count($selectData)){
            $sql .= 'WHERE '.$this->buildWhere($selectData);
        }
        if ((is_array($limit) && count($limit)) || $limit) {
            $sql .= 'LIMIT ' . $this->buildLimit($limit);
        }
        $sql .= ';';
        return $this->pdo->exec($sql);
    }
    public function update($table, $update, $selectData = array(), $limit = array() )
    {
        $sql = 'UPDATE  `' . $table . '` SET ';
        $set = array();
        foreach ($update as $key => $value) {
            if ($key == 'password'){
                $value = md5($value);
            }
            $set[]='`'.$key.'` = ' . $this->pdo->quote($value);
        }

        $sql .= implode(', ', $set);
        if (count($selectData)){
            $sql .= ' WHERE '.$this->buildWhere($selectData);
        }
        if ((is_array($limit) && count($limit)) || $limit) {
            $sql .= ' LIMIT ' . $this->buildLimit($limit);
        }
        $sql .= ';';
        $return = $this->pdo->exec($sql);
        return $return || $return===0 ?true :false;
    }
    public function insert($table, $insertData){

        $var=array();
        $val=array();
        foreach ($insertData as $key=>$item){
            if ($key == 'password'){
                $item = md5($item);
            }
            $var[]=$key;
            $val[]=$this->pdo->quote($item);
        }
        $sql = 'INSERT INTO `'.$table.'` (`'.implode('`, `', $var).'`) VALUES ('.implode(', ', $val).');';

        if ($this->pdo->exec($sql)){
            return \PDO::lastInsertId();
        }
        return false;
    }
}