<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 18.11.2017
 * Time: 14:15
 */

namespace PhoneBook;


class File extends Pattern\Singleton{
    protected static $typeFile = array(
        'image/gif' => array(
            'type' => 'image',
            'ext' => 'gif'
        ),
        'image/jpeg' => array(
            'type' => 'image',
            'ext' => 'jpg'
        ),
        'image/png' => array(
            'type' => 'image',
            'ext' => 'png'
        ),
        'application/x-shockwave-flash' => array(
            'type' => 'image',
            'ext' => 'swf'
        ),
        'image/psd' => array(
            'type' => 'image',
            'ext' => 'psd'
        ),
        'image/bmp' => array(
            'type' => 'image',
            'ext' => 'bmp'
        ),
        'image/tiff' => array(
            'type' => 'image',
            'ext' => 'tiff'
        ),
        'application/octet-stream' => array(
            'type' => 'image',
            'ext' => 'jpc'
        ),
        'image/jp2' => array(
            'type' => 'image',
            'ext' => 'jp2'
        ),
        'image/iff' => array(
            'type' => 'image',
            'ext' => 'iff'
        ),
        'image/vnd.wap.wbmp' => array(
            'type' => 'image',
            'ext' => 'wbmp'
        ),
        'image/xbm' => array(
            'type' => 'image',
            'ext' => 'xbm'
        ),
        'image/vnd.microsoft.icon' => array(
            'type' => 'image',
            'ext' => 'ico'
        ),
        'image/webp' => array(
            'type' => 'image',
            'ext' => 'webp'
        ),
    );
    public function uploadImage($fileInput,$dir,$name,$type = array())
    {
        $error = false;
        $files = array();
        $i = 0;
        $countFilter = count($type);
        foreach( $fileInput as $file ){
            $fileDirName = $dir . $name . '=' . $i;
            if (isset(static::$typeFile[$file['type']])){
                $fileDirName .= '.' . static::$typeFile[$file['type']]['ext'];
            }
            if($countFilter && !in_array($file['type'], $type) || \PhoneBook\Conf::MAX_SIZE_UPLOAD_FILE < $file['size']) {
                $error = true;
            } else {
                if( move_uploaded_file( $file['tmp_name'], ROOT_SITE . $fileDirName ) ) {
                    $files[] = $fileDirName;
                }
                else{
                    $error = true;
                }
            }
            $i++;
        }
        if ($error){
            return false;
        }
        return $files;
    }
}