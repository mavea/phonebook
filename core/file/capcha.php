<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 18.11.2017
 * Time: 14:49
 */

namespace PhoneBook\File;


class Capcha
{
    protected static $chars = 'abdefhknrstyz23456789';
    protected static $capcha = array();
    protected function createCode($param='')
    {
        if (!$param || !isset(static::$capcha[$param])) {
            $numChars = strlen(static::$chars);
            $str = '';
            for ($i = 0; $i < \PhoneBook\Conf::LONG_CAPCHA; $i++) {
                $str .= substr(static::$chars, rand(1, $numChars) - 1, 1);
            }
            if ($param){
                static::$capcha[$param] = $str;
            }else{
                return $str;
            }
        }
        return static::$capcha[$param];
    }
    public function setCapcha($action = '')
    {
        $code = $this->createCode($action);

        global $PhoneBook;
        return $PhoneBook->Common->Typo->crypt(array(
            'action' => $action,
            'code' => $code
        ));
    }
    public function checkCapcha($capcha, $code, $action = '')
    {
        global $PhoneBook;
        if ($code){
            $code = $PhoneBook->Common->Typo->decrypt($code);
            if ($code){
                if ($action == $code->action && strtolower($capcha)  == $code->code){
                    return true;
                }
            }
        }
        return false;
    }
    public function getImage($code)
    {
        global $PhoneBook;
        $PhoneBook->View->setType('file');
        $code = $PhoneBook->Common->Typo->decrypt($code);
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s", 10000) . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Content-Type:image/png");
        $lenCode = strlen($code->code);
        $fontSize = 30;
        $lenImg = $lenCode * $fontSize + 20;
        $heightImg = $fontSize+2;
// создаем изображение
        $im = imagecreate($lenImg, $heightImg);

// Выделяем цвет фона (белый)
        $w=imagecolorallocate($im, 255, 255, 255);

// Выделяем цвет для фона (светло-серый)
        $g1=imagecolorallocate($im, 192, 192, 192);

// Выделяем цвет для более темных помех (темно-серый)
        $g2=imagecolorallocate($im, 64,64,64);

// Рисуем сетку
        for ($i=0;$i<=$lenImg;$i+=5) imageline($im,$i,0,$i,$heightImg,$g1);
        for ($i=0;$i<=$heightImg;$i+=5) imageline($im,0,$i,$lenImg,$i,$g1);

// Выделяем четыре случайных темных цвета для символов
        for ($i=0; $i<$lenCode; $i++){
            imagestring($im,
                5,
                $i*25+10+rand(-10,10),
                7+rand(-5,5),
                strtoupper($code->code{$i}),
                imagecolorallocate($im,rand(0,128),rand(0,128),rand(0,128))
            );
        }

// Выводим пару случайных линий тесного цвета, прямо поверх символов.
// Для увеличения количества линий можно увеличить,
// изменив число выделенное красным цветом
        for ($i=0;$i<8;$i++) imageline($im,rand(0,$lenImg),rand(0,$heightImg),rand(0,$lenImg),rand(0,$heightImg),$g2);


// Коэфициент увеличения/уменьшения картинки
        $k=1.7;

// Создаем новое изображение, увеличенного размера
        $im1=imagecreatetruecolor($lenImg*$k,$heightImg*$k);

// Копируем изображение с изменением рамеров в большую сторону
        imagecopyresized($im1, $im, 0, 0, 0, 0, $lenImg*$k, $heightImg*$k, 101, $heightImg);

// Создаем новое изображение, нормального размера
        $im2=imagecreatetruecolor($lenImg,$heightImg);

// Копируем изображение с изменением рамеров в меньшую сторону
        imagecopyresampled($im2, $im1, 0, 0, 0, 0, $lenImg, $heightImg, $lenImg*$k, $heightImg*$k);

// Генерируем изображение
        imagepng($im2);

// Освобождаем память
        imagedestroy($im2);
        imagedestroy($im1);
        imagedestroy($im);
        exit;
    }

}