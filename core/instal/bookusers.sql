--
-- Структура таблицы `bookusers`
--

CREATE TABLE IF NOT EXISTS `bookusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Таблица пользователей';
