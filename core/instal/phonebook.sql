--
-- Структура таблицы `phonebook`
--

CREATE TABLE IF NOT EXISTS `phonebook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL COMMENT 'хозяин записи',
  `name` varchar(40) NOT NULL COMMENT 'имя',
  `surname` varchar(40) NOT NULL COMMENT 'фамилия',
  `photo` varchar(255) DEFAULT NULL COMMENT 'фото',
  `email` varchar(255) DEFAULT NULL COMMENT 'электронный ящик',
  `telephone` varchar(15) DEFAULT NULL COMMENT 'телефон',
  UNIQUE KEY `id` (`id`),
  KEY `user` (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Телефонные книги';