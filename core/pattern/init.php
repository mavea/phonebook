<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 17.11.2017
 * Time: 13:14
 */

namespace PhoneBook\Pattern;


class Init {
    protected $initInfo = array();
    public function setData($initInfo){
        $this->initInfo = $initInfo;
    }
    public function __construct ($initInfo=array())
    {
        $this->setData($initInfo);
        return $this;
    }
    public function __get($key)
    {
        return isset($this->initInfo[$key]) ?$this->initInfo[$key] :null;
    }
    public function __set($key, $value)
    {
        $this->initInfo[$key] = $value;
    }

    public function __call($key,$value){
        if ($value) {
            return $this->__set($key, $value);
        } else {
            return $this->__get($key);
        }
    }
    public function test(){
        return $this->initInfo;
    }
    public function viewPage($initInfo){
        global $PhoneBook;
        return $PhoneBook->View->viewPage($initInfo);
    }
}