<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 13.11.2017
 * Time: 14:51
 */

namespace PhoneBook\Pattern;


abstract class Singleton {
    static protected $childClass = array();
    static protected $obj = NULL;
    static public function getInstance()
    {
        $class = get_called_class();
        if (is_null(static::$obj[$class])) {
            static::$obj[$class] = new static();
        }
        return static::$obj[$class];
    }
    public function __call($name, $attr){
        $class = (get_called_class()!='PhoneBook\\Core' ?get_called_class() :'PhoneBook' ).'\\'.$name;
        if (isset(static::$obj[$class])){
            return static::$obj[$class];
        }
        if (isset(static::$childClass[$class])){
            if (method_exists(static::$childClass[$class],'getInstance')){
                return call_user_func(array(static::$childClass[$class],'getInstance'), $attr);
            } else {
                return new static::$childClass[$class]($attr);
            }
        }
        if (method_exists($class,'getInstance')){
            return call_user_func(array($class,'getInstance'), $attr);
        } else {
            return new $class($attr);
        }
    }
    public static function __callStatic($name, $attr){
        return call_user_func(array(static::getInstance(),$name), $attr);
    }
    public function __get($name)
    {
        return $this->__call($name, null);
    }
    protected function __construct ()
    {
    }
}