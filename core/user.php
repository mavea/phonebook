<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 13.11.2017
 * Time: 15:14
 */

namespace PhoneBook;


class User extends \PhoneBook\Pattern\Singleton
{
    protected static $selfUser = null;
    static public function __callStatic($name, $attr){
        static::checkSelfUser();
        return call_user_func(array(static::$selfUser,$name), $attr);
    }

    protected function __construct ()
    {
        static::checkSelfUser();
    }
    static protected function checkSelfUser($userData = array())
    {
        if (is_null(static::$selfUser)) {
            static::$selfUser = new User\Init();
            global $PhoneBook;
            if ($PhoneBook->User()->Session()->checkSession()){
                if(!count($userData)){
                    $userData = $PhoneBook->User()->Session()->listSession();
                }
                static::$selfUser->setData($userData);
                return true;
            }
            return false;
        }
        return true;
    }
    public function isAuth()
    {
        global $PhoneBook;
        return $PhoneBook->User->Session->id ?true :false;
    }


    public function checkUser($login){
        global $PhoneBook;
        $us=$PhoneBook->DB->select('bookusers', array('login' => $login), 1);
        if (is_object($us) && $us->fetch()){
            return true;
        }
        return false;
    }
    public function addUser($data){
        global $PhoneBook;
        return $PhoneBook->DB->insert('bookusers', array('login' => $data['login'], 'password' => $data['password']));
    }


    public function authUser($data)
    {
        global $PhoneBook;
        $user = $PhoneBook->DB->select('bookusers', array('login' => $data['login'], 'password' => $data['password']),1);
        if (is_object($user)) {
            $userInfo=$user->fetch();
            if($userInfo) {
                $PhoneBook->User->Session->createSession($userInfo);
                static::$selfUser = null;
                static::checkSelfUser();
                $View = $PhoneBook->View;
                $View->setMessage('session_id', session_id());
                return true;
            }
        }
        return false;
    }
    public function outUser(){
        global $PhoneBook;
        static::$selfUser = null;
        $PhoneBook->User->Session->dropSession();
        return true;
    }
    public function editUser($data){
        global $PhoneBook;
        $send=array();
        if ($data['login']!=static::$selfUser->login){
            $send['login'] = $data['login'];
            $PhoneBook->User->Session->login = $data['login'];
        }
        if ($data['password']){
            $send['password'] = $data['password'];
        }
        if (count($send)){
            return $PhoneBook->DB->update('bookusers', $send, array('id' => $PhoneBook->User->Session->id),1);
        }
        return true;
    }
}