<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 16.11.2017
 * Time: 17:24
 */

namespace PhoneBook\User;


class Controller extends \PhoneBook\User
{
    public function Registration($data)
    {
        global $PhoneBook;
        $View = $PhoneBook->View;
        if (!$PhoneBook->User->isAuth()){
            if (count($data)){
                $Typo = $PhoneBook->Common->Typo;
                switch (false){
                    case $Typo->checkLogin($data['login']):
                        $View->setMessage('status', 406);
                        $View->setMessage('statusType', 'danger');
                        $View->setMessage('message', 'Логин ' . $data['login'] . ' не приемлем');
                        break;
                    case $PhoneBook->File->Capcha->checkCapcha($data['capcha'], $data['capcha_id'], 'register'):
                        $View->setMessage('status', 406);
                        $View->setMessage('statusType', 'danger');
                        $View->setMessage('message', 'Капча указана не верно');
                        break;
                        /*
                    case $Typo->checkMail($data['email']):
                        $View->setMessage('status', 406);
                        $View->setMessage('statusType', 'danger');
                        $View->setMessage('message', 'Емаил ' . $data['email'] . ' не приемлем');
                        break
                        ;*/
                    case $Typo->checkPassword($data['password']):
                        $View->setMessage('status', 406);
                        $View->setMessage('statusType', 'danger');
                        $View->setMessage('message', 'Пароль не приемлем');
                        break;
                    default:
                        if (!$PhoneBook->User->checkUser($data['login']) && $PhoneBook->User->addUser($data)){
                            $View->setMessage('status', 200);
                            $View->setMessage('statusForm', 'user');
                            $View->setMessage('statusType', 'success');
                            $View->setMessage('message', 'Логин ' . $data['login'] . ' успешно зарегистрирован');
                        } else {
                            $View->setMessage('status', 406);
                            $View->setMessage('statusType', 'danger');
                            $View->setMessage('message', 'Логин ' . $data['login'] . ' уже занят');
                        }
                        break;
                }
            }
        } else {
            $View->setMessage('status', 200);
            $View->setMessage('statusForm', 'user');
            $View->setMessage('statusType', 'success');
            $View->setMessage('message', 'Вы уже авторизованы');
        }
        echo $PhoneBook->View->viewPage(\PhoneBook\Conf::VIEW_REGISTER);
    }
    public function Auth($data)
    {
        global $PhoneBook;
        $View = $PhoneBook->View;
        if (!$PhoneBook->User->isAuth()) {
            if ($PhoneBook->Common->Typo->checkLogin($data['login'])) {
                if ($PhoneBook->File->Capcha->checkCapcha($data['capcha'], $data['capcha_id'], 'auth')) {
                    if ($PhoneBook->User->authUser($data)) {
                        $View->setMessage('status', 200);
                        $View->setMessage('statusForm', 'user');
                        $View->setMessage('statusType', 'success');
                        $View->setMessage('message', 'Успешная авторизация');
                    } else {
                        $View->setMessage('status', 424);
                        $View->setMessage('statusType', 'danger');
                        $View->setMessage('message', 'Пара логин/пароль не обнаружена');
                    }
                } else {
                    $View->setMessage('status', 406);
                    $View->setMessage('statusType', 'danger');
                    $View->setMessage('message', 'Капча указана не верно');
                }
            } else {
                if ($data['login']){
                    $View->setMessage('status', 406);
                    $View->setMessage('statusType', 'danger');
                    $View->setMessage('message', 'Логин ' . $data['login'] . ' не приемлем');
                }
            }
        } else {
            $View->setMessage('status', 200);
            $View->setMessage('statusForm', 'user');
            $View->setMessage('statusType', 'success');
            $View->setMessage('message', 'Вы уже авторизованы');
        }
        echo $PhoneBook->View->viewPage(\PhoneBook\Conf::VIEW_AUTH);
    }
    public function Out($data) {
        global $PhoneBook;
        $View = $PhoneBook->View;
        if ($PhoneBook->User->isAuth()) {
            if ($PhoneBook->User->outUser($data)) {

            }

            $View->setMessage('status', 200);
            $View->setMessage('statusForm', 'user');
            $View->setMessage('statusType', 'success');
            $View->setMessage('message', 'Выход успешно осуществлен');
        } else {
            $View->setMessage('status', 200);
            $View->setMessage('statusForm', 'user');
            $View->setMessage('statusType', 'success');
            $View->setMessage('message', 'Вы уже разлогинились');
        }
        echo $PhoneBook->View->viewPage(\PhoneBook\Conf::VIEW_OUT);
    }

    public function Edit($data)
    {
        global $PhoneBook;
        $View = $PhoneBook->View;
        if ($PhoneBook->User->isAuth()){
            if (count($data)){
                $Typo = $PhoneBook->Common->Typo;
                switch (false){
                    case $Typo->checkLogin($data['login']):
                        $View->setMessage('status', 406);
                        $View->setMessage('statusType', 'danger');
                        $View->setMessage('message', 'Логин ' . $data['login'] . ' не приемлем');
                        break;
                    /*
                case $Typo->checkMail($data['email']):
                    $View->setMessage('status', 406);
                    $View->setMessage('statusType', 'danger');
                    $View->setMessage('message', 'Емаил ' . $data['email'] . ' не приемлем');
                    break
                    ;*/
                    case !$data['password'] || $Typo->checkPassword($data['password']):
                        $View->setMessage('status', 406);
                        $View->setMessage('statusType', 'danger');
                        $View->setMessage('message', 'Пароль не приемлем');
                        break;
                    case !($data['login'] != $PhoneBook->User->Session->login && $PhoneBook->User->checkUser($data['login'])):
                        $View->setMessage('status', 406);
                        $View->setMessage('statusType', 'danger');
                        $View->setMessage('message', 'Пользователь с таким логином уже существует');
                        break;
                    default:
                        if ($PhoneBook->User->editUser($data)){
                            $View->setMessage('status', 200);
                            $View->setMessage('statusForm', 'user');
                            $View->setMessage('statusType', 'success');
                            $View->setMessage('message', 'Пользователь ' . $data['login'] . ' успешно сохранен');
                        } else {
                            $View->setMessage('status', 406);
                            $View->setMessage('statusType', 'danger');
                            $View->setMessage('message', 'Ошибка сохранения');
                        }
                        break;
                }
            }
        } else {
            $View->setMessage('status', 200);
            $View->setMessage('statusForm', 'user');
            $View->setMessage('statusType', 'danger');
            $View->setMessage('message', 'Вы не авторизованы');
        }
        echo $PhoneBook->View->viewPage(\PhoneBook\Conf::VIEW_EDIT, static::$selfUser);
    }
}