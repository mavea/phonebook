<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 17.11.2017
 * Time: 0:56
 */

namespace PhoneBook\User;


class Session extends \PhoneBook\Pattern\Singleton{
    protected static $session = array();
    protected static $session_start = false;
    protected function __construct ()
    {
        static::startSession();
    }
    public function checkSession ()
    {
        if (static::$session_start) {
            return true;
        }
        return false;
    }
    public function startSession()
    {
        if (!static::checkSession()){
            static::$session_start = true;
            session_start();
            static::readSession();
        }
    }
    public function readSession ()
    {
         static::$session = $_SESSION;
    }
    public function createSession ($data)
    {
        unset($data['password']);
        static::$session = $data;
    }
    public function dropSession ()
    {
        static::$session_start=false;
        static::$session = $_SESSION = array();
        session_destroy();
    }
    public static function finishSession ()
    {
        $_SESSION = static::$session;
    }

    public function listSession ()
    {
        return static::$session;
    }
    public function __get($name)
    {
        return static::$session[$name]? :null;
    }
    public function __set($name, $value)
    {
        return static::$session[$name] = $value;
    }
    public function test(){
        echo 'dfgdfgdfgdfgdfgdfg';
    }
}