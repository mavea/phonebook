<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 13.11.2017
 * Time: 16:59
 */

namespace PhoneBook;


class View extends \PhoneBook\Pattern\Singleton{
    protected static $type = '';
    protected static $title = '';
    protected static $js = array();
    protected static $css = array();
    protected static $requestV = array();
    protected static $message = array();

    public function setType($type)
    {
        static::$type = $type;
    }

    public static function setMessage($messageKey, $message)
    {
        self::$message[$messageKey] = $message;
    }
    public static function getMessage($name = false)
    {
        if ($name) {
            return self::$message[$name];
        }
        return self::$message;
    }
    public static function setRequest($request)
    {
        self::$requestV = $request;
    }
    public static function Request($name = false)
    {
        if ($name) {
            return self::$requestV[$name];
        }
        return self::$requestV;
    }
    public static function setTitle($title)
    {
        static::$title = $title;
    }
    public static function addJS($js){
        static::$js[$js] = $js;
    }
    public static function addCSS($css){
        static::$css[$css] = $css;
    }
    public static function viewPagesShell($buffer)
    {

        \PhoneBook\User\Session::finishSession();//session

        if (static::$type == 'json' || static::$type == 'file') {
            return $buffer;
        }
        if (static::checkAjax()){
            return json_encode(self::$message);
        }
        //возвращаем правильную дирикторию
        chdir(dirname($_SERVER['SCRIPT_FILENAME']));
        $buffer = str_replace('{{body}}', $buffer, static::viewPage(\PhoneBook\Conf::VIEW_PAGES_SHELL));
        $buffer = str_replace('{{title}}', static::$title, $buffer);
        $js = '';
        if (count(static::$js)) {
            foreach (static::$js as $i) {
                $js .= '<script src="' . $i . '"></script>';
            }
        }
        $buffer = str_replace('{{js}}', $js, $buffer);
        $css = '';
        if (count(static::$css)) {
            foreach (static::$css as $i) {
                $css .= '<link rel="stylesheet" href="'.$i.' />';
            }
        }
        $buffer = str_replace('{{css}}', $css, $buffer);



        return ($buffer);
    }
    public function viewPage($patternPage, $data = NULL)
    {
        if (is_null($data)) {
            global $PhoneBook;
            $data = $PhoneBook->View;
        }
        return \PhoneBook\View\Init::getInstance()->getContent(file_get_contents(ROOT_PROJECT.$patternPage), $data);
    }
    public static function checkAjax(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            return true;
        }
        return false;
    }
    public function isAuth()//костыль, но иначе существенно придется перерабатывать шаблонизатор
    {
        global $PhoneBook;
        return $PhoneBook->User->isAuth() ?1 :0;
    }
    public function getCapchaID($action)
    {
        global $PhoneBook;
        return $PhoneBook->File->Capcha->setCapcha($action);
    }
}