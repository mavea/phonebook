<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 13.11.2017
 * Time: 16:59
 */

namespace PhoneBook\View;


class Init{
    protected $data = array();
    static public function getInstance()
    {
        return new static();
    }
    public function __construct ()
    {
    }
    protected function replace($matches){
        $matches[1] = trim($matches[1]);
        $dataType='';
        if (substr($matches[1],-2) == ');' || substr($matches[1],-1)==')') {
            list($data,$attr) = explode('(', $matches[1], 2);
            $attr = substr($attr,0,(strrpos($attr, ")")));
            $attr = $attr ?explode(',', $attr) :array();
            $dataType='function';
        }else{
            $data = $matches[1];
        }

        if (is_array($this->data) && isset($this->data[$data])){
            return $this->data[$data];
        }
        if (is_object($this->data)) {
                if ($dataType=='function') {
                    return call_user_func_array(array($this->data, $data), $attr);
                }else{
                    if(property_exists($this->data,$data)) {
                        return $this->data->$data;
                    }
                    return $this->data->$data();

                }
        }
        return $matches[0];
    }
    protected function replacePHP($matches){
        if ($matches[2]){
            eval($matches[2]);
        }
        return '';
    }
    public function getContent($pattern,$data)
    {
        $this->data = $data;
        $pattern = preg_replace_callback('/\<\?(php)?(.*?)\?\>/ius',array($this, 'replacePHP'), $pattern);
        $pattern = preg_replace_callback('/\{\{(.*?)\}\}/ius',array($this, 'replace'), $pattern);
        return $pattern;
    }
}
