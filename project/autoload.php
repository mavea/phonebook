<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 13.11.2017
 * Time: 14:42
 */
//define('ROOT_SITE','.'.preg_replace('/[^\/]+/','..',$_SERVER["REQUEST_URI"]));
define('ROOT_SITE','./');
define('ROOT_CORE',ROOT_SITE.'../core/');
define('ROOT_PROJECT',ROOT_SITE.'../project/');

require (ROOT_PROJECT.'conf.php');
spl_autoload_register(function ($name) {
    $name = strtolower(substr($name, 10));
    //echo "Хочу загрузить ".(ROOT_CORE.$name.'.php')."\n";
    require (ROOT_CORE.$name.'.php');
});
ob_start("\\PhoneBook\\View::viewPagesShell");
/**
 * @var \PhoneBook\Core
 */
global $PhoneBook;
$PhoneBook = PhoneBook\Core::getInstance();
PhoneBook\View::setRequest($_REQUEST);
PhoneBook\View::setMessage('status', 200);