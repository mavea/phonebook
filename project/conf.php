<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 13.11.2017
 * Time: 15:25
 */

namespace PhoneBookProject;


abstract class Conf {
    const URL_AUTH = '/auth/';
    const VIEW_AUTH = 'view/user/auth.php';
    const URL_REGISTER = '/register/';
    const VIEW_REGISTER = 'view/user/register.php';
    const VIEW_DEFAULT = 'view/index.php';
    const VIEW_PAGES_SHELL = 'view/pageshell.php';
    const URL_BOOK = '/phonebook/';
    const VIEW_BOOK = 'view/phonebook.php';


    const URL_EDIT = '/edit/';
    const VIEW_EDIT = 'view/user/edit.php';


    const URL_OUT = '/out/';
    const VIEW_OUT = 'view/user/message.php';

    const DB_ADDRESS = 'mysql:host=127.0.0.1;dbname=phoneBook;charset=utf8';
    const DB_USER = 'root';
    const DB_PASSWORD = '';

    const LONG_CAPCHA = 4;
    const URL_CAPCHA = '/style/img/capcha';

    const DIR_UPLOAD_IMAGE = 'upload/photo/';
    const MAX_SIZE_UPLOAD_FILE = 2097152;
}