<?php
\PhoneBook\View::setTitle('Телефонный справочник');
?>
{{viewPage(/view/menu.php)}}
<div class="card text-center">
    <div class="card-header">
        Добро пожаловать в сервис телефонной книги
    </div>
    <div class="card-body">
        <p class="card-text">Для пользования сервисом, вы должны быть зарегистрированны</p>
    </div>
    <div class="card-footer">
        <div class="btn-group" role="group" aria-label="Basic example">
            <a role="button" class="btn btn-secondary" href="/auth/">Авторизация</a>
            <a role="button" class="btn btn-secondary" href="/register/">Регистрация</a>
        </div>
    </div>
</div>
<div class="container">
    <p align="CENTER"><strong>Тестовое задание </strong><span lang="en-US"><strong>PHP</strong></span><strong>+</strong><span lang="en-US"><strong>MySQL+Ajax</strong></span></p>
    <p align="CENTER">Программа - "Телефонная книга".</p>
    <p>Задача:</p>
    <p>Организовать телефонную книгу для пользователей. Любой желающий может зарегистрироваться и создать себе телефонную книгу.</p>
    <p>Организовать авторизацию; загрузку файлов <span lang="en-US">jpg</span>, <span lang="en-US">png</span>;редактирование и отображение информации.</p>
    <p>Страницы:</p>
    <ol>
        <li>
            <p>Страница авторизации</p>
        </li>
        <li>
            <p>Страница регистрации (Требования к логину: только латинские буквы и цифры. Проверка почты на правильность. Требование к паролю: должен содержать и цифры, и буквы.)</p>
        </li>
        <li>
            <p>Страница работы с книгой(все операции без перезагрузки страницы, с помощью ajax)</p>
        </li>
    </ol>
    <p>Таблицы:</p>
    <ol>
        <li>
            <p>Таблица пользователей, поля: логин, пароль и т.д.</p>
        </li>
        <li>
            <p>Таблица с записями книги: данные записей(Имя, Фамилия, телефон, <span lang="en-US">email</span>, фото-записи и т.д&hellip;.)</p>
        </li>
    </ol>
    <p>Функции:</p>
    <ol>
        <li>
            <p>Авторизация</p>
        </li>
        <li>
            <p>Добавление новой записи и загрузка к ней картинки</p>
        </li>
        <li>
            <p>Редактирование существующих записей</p>
        </li>
        <li>
            <p>Отображение, как общего списка, так и отдельных записей, сортировка списка</p>
            <ol>
                <li>
                    <p>создать функцию, которая переводила бы цифровое обозначание цифр в буквенное до числа 999 999999999, например, 21125 =&gt; 'двадцать одна тысяча сто двадцать пять'. Применить ее к отображению телефонного номера в отдельных записях</p>
                </li>
            </ol>
        </li>
        <li>
            <p>Выход</p>
        </li>
    </ol>
    <p>Условия:</p>
    <ol>
        <li>
            <p>Версия PHP 5.5.38</p>
        </li>
        <li>
            <p><a name="_GoBack"></a>Не использовать фреймворки и библиотеки PHP</p>
        </li>
        <li>
            <p>Использовать</p>
            <ol>
                <li>
                    <p lang="en-US">JQuery</p>
                </li>
                <li>
                    <p>Создать простой класс Db (singleton) с использованием PDO для обращений к базе <span lang="en-US">MySQL</span></p>
                </li>
                <li>
                    <p lang="en-US">MVC-подход (разделение как минимум на контроллер и представление)</p>
                </li>
                <li>
                    <p>Для форм авторизации и регистрации проверка Captcha</p>
                </li>
                <li>
                    <p>В качестве основы для оформления использовать Bootstrap http://getbootstrap.com/</p>
                </li>
            </ol>
        </li>
        <li>
            <p>обязательная проверка полей со стороны клиента и сервера</p>
        </li>
        <li>
            <p>Файл картинки не более 2<span lang="en-US">Mb</span>, только <span lang="en-US">jpg</span>,<span lang="en-US">png</span></p>
        </li>
    </ol>
    <p>Результат задания:</p>
    <ol>
        <li>
            <p>Файл <span lang="en-US">db-structure.sql</span></p>
        </li>
        <li>
            <p><span lang="en-US">PHP </span>файлы</p>
        </li>
        <li>
            <p>Сколько времени было потрачено на выполнение задания?</p>
        </li>
    </ol>
</div>