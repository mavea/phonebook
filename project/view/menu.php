<?php
\PhoneBook\View::addJS('/style/js/menu.js');
?><ul class="nav justify-content-end" style="display: none">
    <li class="nav-item">
        <a class="nav-link" href="/">Главная</a>
    </li>
    <li class="nav-item">
        <a class="nav-link not-auth" href="/register/">Регистрация</a>
    </li>
    <li class="nav-item">
        <a class="nav-link not-auth" href="/auth/">Авторизация</a>
    </li>
    <li class="nav-item if-auth">
        <a class="nav-link" href="/phonebook/">Телефонная книга</a>
    </li>
    <li class="nav-item if-auth">
        <a class="nav-link" href="/edit/">Редактировать</a>
    </li>
    <li class="nav-item if-auth">
        <a class="nav-link" href="/out/">Выйти</a>
    </li>
    <input name="isAuth" type="hidden" value="{{isAuth()}}" />
</ul>