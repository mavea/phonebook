<?php
\PhoneBook\View::setTitle('Телефонная книга');
\PhoneBook\View::addJS('/style/js/form.js');
\PhoneBook\View::addJS('/style/js/book.js');
?>
{{viewPage(/view/menu.php)}}


<div class="container" align="center">
    <div class="bookList" align="left">
        <div class="btn-group" role="group">
            <span role="button" class="btn btn-secondary button-create">Завести новую запись</span>
        </div>


        <table class="table table-striped table-dark">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">фамилия</th>
                <th scope="col">имя</th>
                <th scope="col">электронный ящик</th>
                <th scope="col">телефон</th>
            </tr>
            </thead>
            <tbody class="bookListTabl">
            {{listBook(view/phonebook/item.php)}}
            </tbody>
        </table>
    </div>
    <div class="bookInit" style="display: none">
        <form name="BookItem" action="/phonebook/">
            <input name="id" type="hidden" value="" />
            <div class="card" style="width: 40rem;">
                <img class="card-img-top" src="/style/img/foto.jpg" alt="Card image cap">
                <input style="display: none" name="photo" type="file" multiple="multiple" accept="image/*">
                <div class="card-body">
                    <div class="alert" role="alert"></div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2 col-form-label">Фамилия</label>
                        <div class="col-sm-10">
                            <div class="input-group input-group-sm">
                                <input name="surname" type="text" class="form-control compulsory-filling" placeholder="Surname" aria-label="Фамилия" aria-describedby="sizing-addon2" value="">
                                <span class="input-group-addon data-clear">&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Имя</label>
                        <div class="col-sm-10">
                            <div class="input-group input-group-sm">
                                <input name="name" type="text" class="form-control compulsory-filling" placeholder="Name" aria-label="Имя" aria-describedby="sizing-addon2" value="">
                                <span class="input-group-addon data-clear">&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="telephone" class="col-sm-2 col-form-label">Телефон</label>
                        <div class="col-sm-10">
                            <div class="input-group input-group-sm">
                                <input name="telephone" type="text" class="form-control" placeholder="Telephone" aria-label="Телефон" aria-describedby="sizing-addon2" value="">
                                <span class="input-group-addon data-clear">&nbsp;&nbsp;&nbsp;</span>
                            </div><span class="telephoneText"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">EMail</label>
                        <div class="col-sm-10">
                            <div class="input-group input-group-sm">
                                <input name="email" type="text" class="form-control" placeholder="Email" aria-label="EMail" aria-describedby="sizing-addon2" value="">
                                <span class="input-group-addon data-clear">&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer submitPanels" forForm="BookItem">

                    <div class="btn-group" role="group">
                        <span role="button" class="btn btn-secondary button-list">Вернуться к записям</span>
                    </div>

                    <div class="btn-group" role="group">
                        <span role="button" class="btn btn-secondary button-drope">Удалить</span>
                    </div>

                    <div class="btn-group" role="group">
                        <span role="button" class="btn btn-secondary button-submit" style="display: none;" href="#">Сохранить</span>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>