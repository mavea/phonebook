<?php
\PhoneBook\View::setTitle('Авторизация в телефонном справочнике');
\PhoneBook\View::addJS('/style/js/form.js');
?>
{{viewPage(/view/menu.php)}}
<div class="container" align="center">
    <div class="card" style="width: 40rem;">
        <div class="card-header">
            Авторизация
        </div>
        <div class="card-body">
            <div class="alert alert-{{getMessage(statusType)}}" role="alert">
                {{getMessage(message)}}
            </div>
            <form name="authForm" action="/auth/">
                <div class="form-group row">
                    <label for="login" class="col-sm-2 col-form-label">Login</label>
                    <div class="col-sm-10">
                        <div class="input-group input-group-sm">
                            <input name="login" type="text" class="form-control compulsory-filling" placeholder="Login" aria-label="Login" aria-describedby="sizing-addon2" value="{{Request(login)}}">
                            <span class="input-group-addon data-clear">&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                        <div class="input-group input-group-sm">
                            <input name="password" type="password" class="form-control compulsory-filling" placeholder="Password" value="{{Request(password)}}">
                            <span class="input-group-addon data-clear">&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="capcha" class="col-sm-2 col-form-label">Capcha</label>
                    <div class="col-sm-10">
                        <div class="input-group input-group-sm">
                            <input name="capcha" maxlength="4" type="text" class="form-control compulsory-filling" placeholder="Capcha" value="">
                            <span class="input-group-addon"><img src="/style/img/capcha?{{getCapchaID(auth)}}" width="140" height="32"/></span>

                        </div>
                    </div>
                </div><input name="capcha_id" type="hidden" value="{{getCapchaID(auth)}}" />
            </form>
        </div>

        <div class="card-footer submitPanels" forForm="authForm">
            <div class="btn-group" role="group">
                <a role="button" class="btn btn-secondary button-submit" style="display: none;" href="#">Авторизация</a>
            </div>
            <div class="btn-group" role="group">
                <a role="button" class="btn btn-secondary button-clear" href="#">Очистить</a>
                <a role="button" class="btn btn-secondary" href="/register/">Регистрация</a>
            </div>
        </div>
    </div>
</div>