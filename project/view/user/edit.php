<?php
\PhoneBook\View::setTitle('Изменение информации о пользователе в телефонном справочнике');
\PhoneBook\View::addJS('/style/js/form.js');
?>
{{viewPage(/view/menu.php)}}
<div class="container" align="center">
    <div class="card" style="width: 40rem;">
        <div class="card-header">
            Редактирование
        </div>
        <div class="card-body">
            <div class="alert alert-{{getMessage(statusType)}}" role="alert">
                {{getMessage(message)}}
            </div>
            <form name="registrForm" action="/edit/">
                <div class="form-group row">
                    <label for="login" class="col-sm-2 col-form-label">Login</label>
                    <div class="col-sm-10">
                        <div class="input-group input-group-sm">
                            <input name="login" type="text" class="form-control compulsory-filling" placeholder="Login" aria-label="Login" aria-describedby="sizing-addon2" value="{{login()}}">
                            <span class="input-group-addon data-clear">&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                        <div class="input-group input-group-sm">
                            <input name="password" type="password" class="form-control" placeholder="Password" value="{{password}}">
                            <span class="input-group-addon data-clear">&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="card-footer submitPanels" forForm="registrForm">
            <div class="btn-group" role="group">
                <a role="button" class="btn btn-secondary button-submit" style="display: none;" href="#">Изменить</a>
                <a role="button" class="btn btn-secondary button-clear" href="#">Очистить</a>
            </div>
        </div>
    </div>
</div>