<?php
\PhoneBook\View::setTitle('Выход пользователя');
?>
{{viewPage(/view/menu.php)}}
<div class="container" align="center">
    <div class="alert alert-{{getMessage(statusType)}}" role="alert" style="width: 40rem;">
        {{getMessage(message)}}
    </div>
</div>