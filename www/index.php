<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 13.11.2017
 * Time: 14:32
 */

require('./../project/autoload.php');



global $PhoneBook;
$request = $PhoneBook->View->Request();
switch ($_SERVER["REDIRECT_URL"]){
    case \PhoneBook\Conf::URL_REGISTER:
        $PhoneBook->User->Controller->Registration($request);
        break;
    case \PhoneBook\Conf::URL_AUTH:
        $PhoneBook->User->Controller->Auth($request);
        break;
    case \PhoneBook\Conf::URL_EDIT:
        $PhoneBook->User->Controller->Edit($request);
        break;
    case \PhoneBook\Conf::URL_OUT:
        header('Location:/');
        $PhoneBook->User->Controller->Out($request);
        break;
    case \PhoneBook\Conf::URL_CAPCHA:
        $code = $PhoneBook->File->Capcha->getImage($_SERVER["QUERY_STRING"]);
        break;
    case \PhoneBook\Conf::URL_BOOK:
        if (!$PhoneBook->User->isAuth()) {
            header('Location:'.\PhoneBook\Conf::URL_AUTH);
            return ;
        }
        if ($request['id']){
            if (count($request)==1){
                //чтение
                PhoneBook\View::setMessage('return', $PhoneBook->Book->Controller->Read($request));
            }elseif(isset($request['drop'])){
                //удаление
                $PhoneBook->Book->Controller->Drop($request);
            }else{
                //редактирование
                $PhoneBook->Book->Controller->Edit($request);
            }
        } else {
            if (count($request)){
                //создание
                $PhoneBook->Book->Controller->Add($request);
            }
            //листинг
            if (\PhoneBook\View::checkAjax()){
                $PhoneBook->Book->Controller->listBook();
            }else{
                echo $PhoneBook->View->viewPage(\PhoneBook\Conf::VIEW_BOOK,$PhoneBook->Book->Controller);
            }
        }
        break;
    default:
        echo $PhoneBook->View->viewPage(\PhoneBook\Conf::VIEW_DEFAULT);
        break;
}