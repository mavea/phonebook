/**
 * Created by Виталий on 17.11.2017.
 */
function readBookItem($id){
    $('.bookList').hide();
    $('.button-drope').hide();
    $.post(
        '/phonebook/',
        {id:$id},
        function( data, status ) {
            if (data.status=200){
                $('.bookInit').show();
                var form = $('form[name=BookItem]');
                form.find('[name=email]').val(data.return.email).trigger('change');
                form.find('[name=id]').val(data.return.id);
                form.find('[name=name]').val(data.return.name).trigger('change');
                form.find('[name=surname]').val(data.return.surname).trigger('change');
                form.find('[name=telephone]').val(data.return.telephone).trigger('change');
                if (!data.return.photo){
                    data.return.photo ='style/img/foto.jpg';
                }
                form.find('input[name=photo]').val('');
                form.find('.card-img-top').attr('src','/' + data.return.photo + '?' + Date.now());
                form.find('span.telephoneText').text(data.return.telephoneText);
                $('.button-drope').show();
            } else {
                alert(data.message)
            }
        }, "json");
}
function setFunctionBookItem(){
    $('.itemBook').click(function(){
        readBookItem($(this).find('th').text());
    });
}
$(document).ready(function(){
    $('.card-img-top').click(function(){
        $(this).closest('form').find('input[name=photo]').trigger('click');
    });
    $('.button-create').click(function(){
        $('.bookList').hide();
        $('.button-drope').hide();
        var form = $('form[name=BookItem]');
        form.find('.card-img-top').attr('src','/style/img/foto.jpg');
        form.find('input[name=photo]').val('');
        form.find('input[name=id]').val('');
        form.find('span.telephoneText').text('');
        clearForm(form);
        $('.bookInit').show();
    });
    $('.button-list').click(function(){
        files = '';
        $('.bookInit').hide();

        $('.alert').removeClass('alert-success alert-danger').html('');
        $.post(
            '/phonebook/',
            {},
            function( data, status ) {
                var table='';
                for (var i = 0; i < data.result.length; i++) {
                    table += '<tr class="itemBook"> <th scope="row">'+data.result[i]['id']+'</th><td>'+data.result[i]['surname']+'</td><td>'+data.result[i]['name']+'</td><td>'+data.result[i]['email']+'</td><td>'+data.result[i]['telephone']+'</td></tr>';
                }
                $('.bookListTabl').html(table);
                setFunctionBookItem()
            }, "json");
        $('.bookList').show();
    });
    $('.button-drope').click(function(){

        $.post(
            '/phonebook/',
            {drop:'true',id:$('form[name=BookItem]').find('input[name=id]').val()},
            function( data, status ) {
                if (data.status==200){
                    alert (data.message);
                    $(".button-list").trigger("click")
                } else {
                    alert(data.message)
                }
            }, "json");
    });
    setFunctionBookItem();

});