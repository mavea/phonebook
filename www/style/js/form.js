/**
 * Created by Виталий on 16.11.2017.
 */
var files;
function clearForm(objForm){
    objForm.trigger('reset');
    var elements = objForm.find("input");
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).trigger('change');
    }
}

$(document).ready(function(){
    function styleTrueOrFalse(bool,obj){
        var obStatus = obj.parent().find('.input-group-addon');
        if (obj.val()){
            if (bool){
                obj.addClass('value-true');
                obStatus.removeClass('icon-false bg-danger data-clear').addClass('icon icon-true bg-success');
            }else{
                obj.removeClass('value-true');
                obStatus.removeClass('icon-true bg-success data-clear').addClass('icon icon-false bg-danger');
            }
        }else{
            clearTrueOrFalse(obj)
        }

        if (obj.hasClass('compulsory-filling')) {
            checkSubmitPanels(obj.closest('form'));
        }
    }
    function checkSubmitPanels(form){
        var submitPanels = $('[forForm="'+form.attr('name')+'"]').find('.button-submit');
        var r = 0;
        var elements = form.find(".compulsory-filling");
        for (var i = 0; i < elements.length; i++) {
            if (! $(elements[i]).hasClass('value-true')) {
                r = r + 1;
            }
        }
        if (r){
            submitPanels.hide();
        } else {
            submitPanels.show();
        }
    }
    function clearTrueOrFalse(obj){
        obj.removeClass('value-true');
        obj.parent().find('.input-group-addon').removeClass('icon icon-true  icon-false  bg-danger bg-success').addClass('data-clear');
    }
    $("[placeholder=Email]").change(function() {
        styleTrueOrFalse(core.typo.checkMail($(this).val()), $(this));
        })
        .focusout(function() {
            $(this).change();
        })
        .focus(function() {
            clearTrueOrFalse($(this));
        });


    $("[placeholder=Surname]").change(function() {
        styleTrueOrFalse(core.typo.checkNames($(this).val()), $(this));
    })
        .focusout(function() {
            $(this).change();
        })
        .focus(function() {
            clearTrueOrFalse($(this));
        });
    $("[placeholder=Capcha]").change(function() {
        styleTrueOrFalse(core.typo.capcha($(this).val()), $(this));
    })
        .focusout(function() {
            $(this).change();
        })
        .focus(function() {
            clearTrueOrFalse($(this));
        });

    $("[placeholder=Name]").change(function() {
        styleTrueOrFalse(core.typo.checkNames($(this).val()), $(this));
    })
        .focusout(function() {
            $(this).change();
        })
        .focus(function() {
            clearTrueOrFalse($(this));
        });

    $("[placeholder=Telephone]").change(function() {
        styleTrueOrFalse(core.typo.checkPhone($(this).val()), $(this));
    })
        .focusout(function() {
            $(this).change();
        })
        .focus(function() {
            clearTrueOrFalse($(this));
        });

    $("[placeholder=Password]").change(function() {
        styleTrueOrFalse(core.typo.checkPassword($(this).val()), $(this));
        })
        .focusout(function() {
            $(this).change();
        })
        .focus(function() {
            clearTrueOrFalse($(this));
        });
    $("[placeholder=Login]").change(function() {
        styleTrueOrFalse(core.typo.checkLogin($(this).val()), $(this));
        })
        .focusout(function() {
            $(this).change();
        })
        .focus(function() {
            clearTrueOrFalse($(this));
        });
    $('.button-clear').click(function() {
        files = '';
        var objForm = $('form[name=' + $(this).closest('.submitPanels').attr('forForm') + ']');
        clearForm(objForm);
    });
    $('input[type=file]').change(function(){
        files = this.files;
    });
    $('.button-submit').click(function() {
        var objForm = $('form[name=' + $(this).closest('.submitPanels').attr('forForm') + ']');
        var data = new FormData();
        $.each( files, function( key, value ){
            data.append( key, value );
        });
        var inputType = [
            'input',
            'textarea',
            'select'
        ];
        var elements = [];
        var i=0;
        for (var y = 0; y < inputType.length; y++) {
            elements = objForm.find(inputType[y]);
            for ( i = 0; i < elements.length; i++) {
                data.append( $(elements[i]).attr('name'), $(elements[i]).val() );
            }
        }

        // Отправляем запрос

        $.ajax({
            url: objForm.attr('action'),
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Не обрабатываем файлы (Don't process the files)
            contentType: false, // Так jQuery скажет серверу что это строковой запрос
            success: function( data, status, jqXHR ){

                $('.alert').removeClass('alert- alert-danger alert-success').addClass('alert-'+data.statusType).html(data.message);
                if (data.status==200) {
                    if (data.statusForm=='user') {
                        setTimeout( 'location="/";', 2000 );
                    }else{
                        setTimeout('$(".button-list").trigger("click")', 2000);
                    }
                }
                /*
                // Если все ОК

                if( typeof respond.error === 'undefined' ){
                    // Файлы успешно загружены, делаем что нибудь здесь

                    // выведем пути к загруженным файлам в блок '.ajax-respond'

                    var files_path = respond.files;
                    var html = '';
                    $.each( files_path, function( key, val ){ html += val +'<br>'; } )
                    //$('.ajax-respond').html( html );'
                    alert(html);
                }
                else{
                    console.log('ОШИБКИ ОТВЕТА сервера: ' + respond.error );
                }*/
            }
        });














        /*

        $.post(
            objForm.attr('action'),
            send,
            function( data, status ) {
                $('.alert').removeClass('alert- alert-danger alert-success').addClass('alert-'+data.statusType).html(data.message);
                if (data.status==200) {


                    if (data.statusForm=='user') {
                        setTimeout( 'location="/";', 2000 );
                    }else{
                        setTimeout('$(".button-list").trigger("click")', 2000);
                    }







                }
        }, "json");*/
    });

    var elements = $('form').find("input");
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).trigger('change');
    }
});