/**
 * Created by Виталий on 17.11.2017.
 */

$(document).ready(function(){
    function showMenu(){
        var flag = $('input[name=isAuth]');
        var obj = flag .parent()
        if (flag.val()=='1') {
            obj.find('.if-auth').show();
            obj.find('.not-auth').hide();
        } else {
            obj.find('.if-auth').hide();
            obj.find('.not-auth').show();
        }
        obj.show();
    }
    showMenu()
});