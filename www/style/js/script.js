/**
 * Created by Виталий on 16.11.2017.
 */
'use strict';
class Typo {
    constructor() {
    }
    checkLogin(login) {
        if(login.search(/[a-zA-Z]/) != -1 && login.search(/[0-9]/) != -1) {
            if(login.search(/[^a-zA-Z0-9]/) != -1) {
                return false;
            }
            return true;
        }
        return false;
    }
    checkPassword(pass) {
        if(pass.search(/[a-zA-Zа-яА-ЯёЁ]/) != -1 && pass.search(/[0-9]/) != -1) {
            return true;
        }
        return false;
    }
    checkMail(mail) {
        if(mail.search(/^[^@]+@[^@\.]+\.[^@]+$/) != -1){
            return true;
        }
        return false;
    }
    capcha(capcha) {
        if(capcha.length == 4){
            return true;
        }
        return false;
    }

    checkNames(names) {
        if(names.search(/[^a-zA-Zа-яА-ЯёЁ]/) != -1) {
            return false;
        }
        return true;
    }
    checkPhone(names) {
        if(names.search(/[^0-9]/) != -1) {
            return false;
        }
        return true;
    }
}
class Core {
    constructor() {
        this.typo = new Typo();
    }
}
let core = new Core();